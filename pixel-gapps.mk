# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# /app
PRODUCT_PACKAGES += \
    FaceLock \
    GoogleCamera \
    GoogleContactsSyncAdapter \
    GoogleTTS \
    GoogleVrCore \
    talkback \
    WallpapersBReel18

PRODUCT_PACKAGES += \
    com.google.android.maps \
    com.google.android.media.effects \
    com.google.android.dialer.support

# /priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    AndroidPlatformServices \
    CarrierSetup \
    ConfigUpdater \
    ConnMetrics \
    GmsCoreSetupPrebuilt \
    GoogleBackupTransport \
    GoogleExtServices \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    Phonesky \
    PrebuiltGmsCorePi \
    DynamiteModulesA \
    AdsDynamite \
    DynamiteModulesC \
    CronetDynamite \
    DynamiteLoader \
    GoogleCertificates \
    MapsDynamite \
    SetupWizard \
    Turbo \
    Wellbeing \
    Velvet

# /symlinks
PRODUCT_PACKAGES += \
    libfacenet.so \
    libgdx.so \
    libjpeg.so

#telephony permissions
PRODUCT_COPY_FILES_OVERRIDES += \
    vendor/pixelgapps/etc/permissions/com.google.android.dialer.support.xml:system/etc/permissions/com.google.android.dialer.support.xml \
    vendor/pixelgapps/etc/sysconfig/dialer_experience.xml:system/etc/sysconfig/dialer_experience.xml

$(call inherit-product, vendor/pixelgapps/pixel-gapps-blobs.mk)
